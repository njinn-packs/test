class UploadFile():
    def run(self):
        fo = self._njinn.upload_file(self.file_path)
        return {"data": fo}

class PrintFile():
    def run(self):
        with open(self.file_path) as data:
            print(data.read())
